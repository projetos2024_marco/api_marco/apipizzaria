//Obtendo conexão com o banco de dados
const connect = require ("../db/connect");

module.exports = class clienteController{
     static async createCliente(req, res){
        const{
            telefone,
            nome,
            cpf,
            logradouro,
            numero,
            complemento,
            bairro,
            cidade,
            estado,
            cep,
            referencia
        } = req.body;
        //Verificando se o atributo chave é diferente de 0(zero)
        if(telefone !== 0){
            const query = `insert into cliente (telefone,nome,cpf,logradouro,numero,complemento,bairro,cidade,estado,cep,referencia) values (
                '${telefone}', 
                '${nome}',
                '${cpf}',
                '${logradouro}',
                '${numero}',
                '${complemento}',
                '${bairro}',
                '${cidade}',
                '${estado}',
                '${cep}',
                '${referencia}'
            )`;//Fim da query

            try{
                connect.query(query, function(err){
                    if(err){
                        console.log(err);
                        res.status(500).json({error: "Usuario não cadastrado no banco!!!"});
                        return;
                    }
                    console.log("Inserido no banco");
                    res.status(201).json({message: "Usuario criado com sucesso"});
                })
            }catch(error){
                console.log("Erro ao executar o insert !!!", error);
                res.status(500).json({error: "Erro interno do servidor !!!"});

            }//Fim do catch
        
        }//Fim do IF
        else{
            res.status(400).json({message:"O telefone é obrigatorio !!!"});

        }//Fim do else

     }//Fim da createCiente
    //select da tabela cliente
    static async getAllClientes(req, res){
        const query = `select * from cliente`;

        try{
            connect.query(query, function(err, data){
                if(err){
                    console.log(err);
                    res.status(500).json({error: "Usuários não encontrados no banco!!!"});
                    return;
                }//fim do if
                let clientes = data;
                console.log("Consulta realizada com sucesso!!!");
                res.status(201).json({clientes});
            });//fim do connect query

        }catch(error){
            console.error("Erro ao executar a consulta: ", error);
            res.status(500).json({error: "Erro interno do servidor!!!"})
        }//fim do catch
    }//fim do getAllClientes
    
    static async getAllClientes2(req,res){


        const {filtro, ordenacao} = req.body;
        

    }
} //Fim do module