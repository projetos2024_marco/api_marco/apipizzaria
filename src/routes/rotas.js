const router = require("express").Router();
const dbController = require("../controller/dbController");
const clienteController = require("../controller/clienteController")
const pizzariaController = require("../controller/pizzariaContoller")

//rota para consulta das tabelas do banco
router.get("/tables/", dbController.getNameTables);
router.get("/tablesdescription/", dbController.getTablesDescription);

//rota para cadastro de clientes]
router.post("/postCliente/", clienteController.createCliente)

//rota para select da tabela cliente
router.get("/clientes/", clienteController.getAllClientes);

//rota para listar as pizzas
router.get("/listPizza/", pizzariaController.listarPedidosPizzas);
router.get("/listPizzaJoin/", pizzariaController.listarPedidosPizzasComJoin);

module.exports = router;    